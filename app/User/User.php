<?php

namespace PONDIT\User;

use PDO;
use PDOException;

class User

    {
        public $userName;
        private $conn;


        public function __construct()
        {
            try{

               session_start();

               $this->conn = new PDO("mysql:host=localhost; dbname=phpb2", "root", "11111");

               $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            }catch(PDOException $e){

                echo $e->getMessage();

            }
        }


        public function index()
        {
           $query = "SELECT * FROM users";

           $stmt = $this->conn->query($query);

            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $user;

            
        }

        public function setData(array $data=[])

        {
            if(array_key_exists('user_name',$data)){

                $this->userName = $data['user_name'];

            }else{

                echo 'User Name Required';
                die();

                

            }
            return $this;


            
            

        }
       public function store()
       {
           try{

            $query = "INSERT INTO users(name) VALUES(:username)";

            $stmt = $this->conn->prepare($query);

            $stmt->execute(array(
                ':username' => $this->userName,
                
            ));
            $_SESSION['status'] = 'Inserted Sucessfully';

            header("Location: index.php");



           }
           catch(PDOException $exception){

                echo $exception->getMessage();
                echo $exception->getLine();


           }


       }
       public function show($id)
       {
          $query = "SELECT * FROM users where id=".$id;

          $stmt = $this->conn->query($query);

          $user = $stmt->fetch(PDO::FETCH_ASSOC);

          //echo'<pre>';

          //print_r($data);
          //die();
          return $user;

           


       }
       public function update($id)
       {
        try {

            $query = $query = "UPDATE users SET name=:username  where id=".$id;

            $stmt = $this->conn->prepare($query);

            $stmt->execute(array(
                ':username' => $this->userName,

            ));
            $_SESSION['status'] = 'Updated Sucessfully';

            header("Location: index.php");
        } catch (PDOException $exception) {

            echo $exception->getMessage();
            echo $exception->getLine();
        }
           

           //die('This is Update Method');
       }
       public function delete($id)
    {
        try {

            $query = $query = "Delete from users where id=" . $id;

            $stmt = $this->conn->prepare($query);

            $stmt->execute(array(
                ':username' => $this->userName,

            ));
            $_SESSION['status'] = 'Deleted Sucessfully';

            header("Location: index.php");
        } catch (PDOException $exception) {

            echo $exception->getMessage();
            echo $exception->getLine();
        }
           
      


       }
       
       
    }
